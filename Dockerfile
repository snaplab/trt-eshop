# Dockerfile
FROM seapy/ruby:2.2.0
MAINTAINER ChangHoon Jeong <iamseapy@gmail.com>

RUN apt-get update

# Install nodejs
RUN apt-get install -qq -y nodejs

# Intall software-properties-common for add-apt-repository
RUN apt-get install -qq -y software-properties-common

# Install Nginx.
RUN add-apt-repository -y ppa:nginx/stable
RUN apt-get update
RUN apt-get install -qq -y nginx=1.8.0-1+trusty1
RUN echo "\ndaemon off;" >> /etc/nginx/nginx.conf
RUN chown -R www-data:www-data /var/lib/nginx
# Add default nginx config
ADD nginx-sites.conf /etc/nginx/sites-enabled/default

# Install foreman
RUN gem install foreman

# Rails App directory
WORKDIR /trt-eshop

# Add default unicorn config
ADD /config/unicorn.rb /trt-eshop/config/unicorn.rb

# Add default foreman config
ADD Procfile /trt-eshop/Procfile

ENV RAILS_ENV production

CMD bundle exec rake assets:precompile && foreman start -f Procfile

# Add here your preinstall lib(e.g. imagemagick, mysql lib, pg lib, ssh config)
RUN apt-get update

RUN apt-get -qq -y install libmagickwand-dev imagemagick git

RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main" > /etc/apt/sources.list.d/pgdg.list && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get install -y --force-yes libpq-dev

#(required) Install Rails App

ADD Gemfile /trt-eshop/Gemfile
ADD Gemfile.lock /trt-eshop/Gemfile.lock
RUN bundle install --jobs 4 --without development test
ADD . /trt-eshop

#(required) nginx port number
EXPOSE 80
